#pragma once

#include <routing/ip.hpp>
#include <routing/packets/base_packet.hpp>

namespace jaf::routing
{
    struct data_packet
        : base_packet<4>
    {
        ip sender_;
        ip receiver_;
        std::vector<std::byte> data_;

        explicit data_packet();
        explicit data_packet(ip sender, ip receiver, std::vector<std::byte> data);

        serialize::stream& archive(serialize::stream& s) final;
    };

    bool operator==(const data_packet& a, const data_packet& b);
}
