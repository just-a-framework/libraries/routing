#pragma once

#include <routing/mask.hpp>
#include <routing/packets/base_packet.hpp>

namespace jaf::routing
{
    struct failure_config_packet
        : base_packet<3>
    {
        explicit failure_config_packet();

        serialize::stream& archive(serialize::stream& s) final;
    };

    bool operator==(const failure_config_packet& a, const failure_config_packet& b);
}
