#pragma once

#include <routing/base_router.hpp>

namespace jaf::routing
{
    struct core_router
        : base_router
    {
        using base_router::handle;

        core_router(uint8_t hwid, uint32_t max_clients = 8);

        void handle(socket& s, const data_packet& p) final;
        void handle(socket& s, const broadcast_packet& p) final;

        void send(const ip& receiver, const std::vector<std::byte>& data) final;
        void broadcast(const std::vector<std::byte>& data) final;
    };
}
