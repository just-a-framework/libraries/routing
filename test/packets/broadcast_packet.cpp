#include <routing/packets/broadcast_packet.hpp>

#include <serialize/streams/vector.hpp>

namespace jaf::routing::test::broadcast_packet
{
    struct broadcast_packet
        : ::jaf::testing::test_suite
    {
    };

    TEST_F(broadcast_packet, archive)
    {
        auto b = std::vector<std::byte>{};
        auto s = ::jaf::serialize::streams::vector{ b };
        
        const auto c = [](const unsigned char i) -> std::byte { return static_cast<std::byte>(i); };

        const auto data = std::vector<std::byte>{ c(0), c(6), c(0), c(1), c(9), c(1) };
        const auto p1 = ::jaf::routing::broadcast_packet{ {5, 6, 7, 8}, data };
        static_cast<::jaf::serialize::ostream&>(s) & p1;

        auto p2 = ::jaf::routing::broadcast_packet{};
        static_cast<::jaf::serialize::istream&>(s) & p2;

        EXPECT_EQ(p1.sender_, p2.sender_);
        EXPECT_EQ(p1.data_, p2.data_);
    }
}
