#include <routing/ip.hpp>

namespace jaf::routing
{
    ip::ip(uint32_t i)
        : ip_{ i }
    {
    }

    ip::ip(uint8_t p1, uint8_t p2, uint8_t p3, uint8_t p4)
        : ip_{ static_cast<uint32_t>(p1 << 24) + (p2 << 16) + (p3 << 8) + (p4 << 0) }
    {
    }

    serialize::stream& ip::archive(serialize::stream& s)
    {
        return (s & ip_);
    }

    ip ip::next() const
    {
        return ip{ ip_ + 1 };
    }
    
    std::ostream& operator<<(std::ostream& os, const ip& i)
    {
        const auto pretty = [i](uint8_t ind)
        {
            return static_cast<int>(static_cast<uint8_t>(i.ip_ >> ind));
        };

        return (os << pretty(24) << "." << pretty(16) << "." << pretty(8) << "." << pretty(0));
    }
    
    bool operator==(const ip& a, const ip& b)
    {
        return a.ip_ == b.ip_;
    }

    bool operator!=(const ip& a, const ip& b)
    {
        return a.ip_ != b.ip_;
    }

    bool operator<(const ip& a, const ip& b)
    {
        return a.ip_ < b.ip_;
    }
    
    bool operator<=(const ip& a, const ip& b)
    {
        return a.ip_ <= b.ip_;
    }

    bool operator>(const ip& a, const ip& b)
    {
        return a.ip_ > b.ip_;
    }

    bool operator>=(const ip& a, const ip& b)
    {
        return a.ip_ >= b.ip_;
    }
}
