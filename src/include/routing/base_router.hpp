#pragma once

#include <routing/ip.hpp>
#include <routing/mask.hpp>
#include <routing/router.hpp>

namespace jaf::routing
{
    struct base_router
        : router
    {
        base_router(uint8_t hwid, uint32_t max_clients = 8);

        void handle(socket& s, const register_hw_packet& p) final;
        void handle(socket& s, const unregister_hw_packet& p) final;
        void handle(socket& s, const address_config_packet& p) final;
        void handle(socket& s, const failure_config_packet& p) final;

    protected:
        const uint8_t hwid_;
        std::optional<ip> ip_;

        void configure_subnetworks(mask m);

        bool send_internal(const ip& receiver, const std::vector<std::byte>& data);
        void broadcast_internal(const broadcast_packet& data);
        bool handle_internal(socket& s, const data_packet& p);
    
    private:
        using socket_t = std::pair<mask, std::reference_wrapper<socket>>;

        std::vector<mask> available_{};
        std::map<uint8_t, socket_t> used_;
        std::optional<mask> m_;
    };
}
