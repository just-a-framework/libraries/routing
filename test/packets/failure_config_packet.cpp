#include <routing/packets/failure_config_packet.hpp>

#include <serialize/streams/vector.hpp>

namespace jaf::routing::test::failure_config_packet
{
    struct failure_config_packet
        : ::jaf::testing::test_suite
    {
    };

    TEST_F(failure_config_packet, archive)
    {
        auto b = std::vector<std::byte>{};
        auto s = ::jaf::serialize::streams::vector{ b };
        
        const auto p1 = ::jaf::routing::failure_config_packet{};
        static_cast<::jaf::serialize::ostream&>(s) & p1;

        auto p2 = ::jaf::routing::failure_config_packet{};
        static_cast<::jaf::serialize::istream&>(s) & p2;
    }
}
