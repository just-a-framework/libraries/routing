#include <routing/ip.hpp>

namespace jaf::routing::test::ip
{
    struct ip
        : ::jaf::testing::test_suite
    {
    };
    
    TEST_F(ip, next_basic)
    {
        const auto i1 = ::jaf::routing::ip{1, 2, 3, 4};
        const auto i2 = ::jaf::routing::ip{1, 2, 3, 5};

        EXPECT_EQ(i1.next(), i2);
    }

    TEST_F(ip, next_overflow)
    {
        const auto i1 = ::jaf::routing::ip{1, 2, 3, 255};
        const auto i2 = ::jaf::routing::ip{1, 2, 4, 0};

        EXPECT_EQ(i1.next(), i2);
    }

    TEST_F(ip, next_more_overflow)
    {
        const auto i1 = ::jaf::routing::ip{1, 255, 255, 255};
        const auto i2 = ::jaf::routing::ip{2, 0, 0, 0};

        EXPECT_EQ(i1.next(), i2);
    }
}
