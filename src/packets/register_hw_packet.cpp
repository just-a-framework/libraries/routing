#include <routing/packets/register_hw_packet.hpp>

namespace jaf::routing
{
    register_hw_packet::register_hw_packet() = default;

    register_hw_packet::register_hw_packet(uint8_t hwid)
        : hwid_{ hwid }
    {
    }

    serialize::stream& register_hw_packet::archive(serialize::stream& s)
    {
        return (s & hwid_);
    }

    bool operator==(const register_hw_packet& a, const register_hw_packet& b)
    {
        return a.hwid_ == b.hwid_;
    }
}
