#pragma once

#include <routing/socket.hpp>
#include <routing/ip.hpp>
#include <signal/listener.hpp>
#include <signal/publisher.hpp>

namespace jaf::routing
{
    struct router;

    struct router_listener
    {
        virtual void handle(router&, const ip& sender, const std::vector<std::byte>& data) = 0;

        virtual ~router_listener();
    };

    struct router
        : signal::listener<socket>
        , signal::publisher<router, router_listener>
    {
        virtual void send(const ip& receiver, const std::vector<std::byte>& data) = 0;
        virtual void broadcast(const std::vector<std::byte>& data) = 0;

        virtual ~router();
    };
}
