#pragma once

#include <routing/mask.hpp>
#include <routing/packets/base_packet.hpp>

namespace jaf::routing
{
    struct unregister_hw_packet
        : base_packet<1>
    {
        uint8_t hwid_ = {0};

        explicit unregister_hw_packet();
        explicit unregister_hw_packet(uint8_t hwid_);

        serialize::stream& archive(serialize::stream& s) final;
    };

    bool operator==(const unregister_hw_packet& a, const unregister_hw_packet& b);
}
