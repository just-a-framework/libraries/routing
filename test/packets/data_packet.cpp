#include <routing/packets/data_packet.hpp>

#include <serialize/streams/vector.hpp>

namespace jaf::routing::test::data_packet
{
    struct data_packet
        : ::jaf::testing::test_suite
    {
    };

    TEST_F(data_packet, archive)
    {
        auto b = std::vector<std::byte>{};
        auto s = ::jaf::serialize::streams::vector{ b };
        
        const auto c = [](const unsigned char i) -> std::byte { return static_cast<std::byte>(i); };

        const auto data = std::vector<std::byte>{ c(0), c(6), c(0), c(1), c(9), c(1) };
        const auto p1 = ::jaf::routing::data_packet{ {1, 2, 3, 4}, {5, 6, 7, 8}, data };
        static_cast<::jaf::serialize::ostream&>(s) & p1;

        auto p2 = ::jaf::routing::data_packet{};
        static_cast<::jaf::serialize::istream&>(s) & p2;

        EXPECT_EQ(p1.sender_, p2.sender_);
        EXPECT_EQ(p1.receiver_, p2.receiver_);
        EXPECT_EQ(p1.data_, p2.data_);
    }
}
