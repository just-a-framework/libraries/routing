#include <routing/core_router.hpp>
#include <serialize/streams/vector.hpp>

namespace jaf::routing::test::core_router
{
    struct core_router
        : ::jaf::testing::test_suite
    {
    };

    struct mock_socket
        : ::jaf::routing::socket
    {
        MOCK_METHOD(void, send_data, (const std::byte*, size_t), ());
    };

    struct mock_router_listener
        : ::jaf::routing::router_listener
    {
        MOCK_METHOD(void, handle, (::jaf::routing::router&, const ::jaf::routing::ip&, const std::vector<std::byte>&), ());
    };

    TEST_F(core_router, handle_data_publish)
    {
        using ::testing::_;
        using ::testing::Invoke;

        const auto c = [](auto i) { return static_cast<std::byte>(i); };

        auto s = mock_socket{};
        auto r = std::make_shared<::jaf::routing::core_router>(0);
        auto rl = std::make_shared<mock_router_listener>();
        r->subscribe(rl);

        EXPECT_CALL(*rl, handle(_, _, _))
            .Times(1)
            .WillOnce(Invoke([&](auto&, const auto& s, const auto& d)
            {
                const auto exps = ::jaf::routing::ip{1, 2, 3, 4};
                EXPECT_EQ(s, exps);
                const auto expd = std::vector<std::byte>{c(1), c(2), c(3), c(4), c(5)};
                EXPECT_EQ(d, expd);
            }));
        
        const auto p = ::jaf::routing::data_packet{{1, 2, 3, 4}, {0, 0, 0, 0}, {c(1), c(2), c(3), c(4), c(5)}};
        r->handle(s, p);
    }

    TEST_F(core_router, handle_data_delegate)
    {
        using ::testing::_;
        using ::testing::Invoke;

        const auto c = [](auto i) { return static_cast<std::byte>(i); };

        auto r = ::jaf::routing::core_router{ 0 };
        auto s1 = mock_socket{};
        auto s2 = mock_socket{};

        r.handle(s1, ::jaf::routing::register_hw_packet{ 1 });
        r.handle(s2, ::jaf::routing::register_hw_packet{ 2 });
        
        EXPECT_CALL(s2, send_data(_, _))
            .Times(1)
            .WillOnce(Invoke([&](const auto* b, auto s)
            {
                auto bf = std::vector<std::byte>(b, b + s);
                auto sr = ::jaf::serialize::streams::vector{ bf };

                auto id = ::jaf::routing::packet_id_t{};
                static_cast<::jaf::serialize::istream&>(sr) & id;
                ASSERT_EQ(id, 4);

                auto p = ::jaf::routing::data_packet{};
                static_cast<::jaf::serialize::istream&>(sr) & p;
                
                const auto exps = ::jaf::routing::ip{1, 2, 3, 4};
                EXPECT_EQ(p.sender_, exps);
                const auto expr = ::jaf::routing::ip{201, 4, 99, 2};
                EXPECT_EQ(p.receiver_, expr);
                const auto expd = std::vector<std::byte>{c(1), c(2), c(3), c(4), c(5)};
                EXPECT_EQ(p.data_, expd);
            }));
        
        const auto p = ::jaf::routing::data_packet{{1, 2, 3, 4}, {201, 4, 99, 2}, {c(1), c(2), c(3), c(4), c(5)}};
        r.handle(s1, p);
    }

    TEST_F(core_router, handle_data_no_delegate)
    {
        using ::testing::_;
        using ::testing::Invoke;

        const auto c = [](auto i) { return static_cast<std::byte>(i); };

        auto r = ::jaf::routing::core_router{ 0 };
        auto s1 = mock_socket{};
        auto s2 = mock_socket{};

        r.handle(s1, ::jaf::routing::register_hw_packet{ 1 });
        r.handle(s2, ::jaf::routing::register_hw_packet{ 2 });
        
        const auto p = ::jaf::routing::data_packet{{1, 2, 3, 4}, {3, 5, 1, 2}, {c(1), c(2), c(3), c(4), c(5)}};
        EXPECT_THROW(r.handle(s1, p), std::invalid_argument);
    }

    TEST_F(core_router, handle_broadcast)
    {
        using ::testing::_;
        using ::testing::Invoke;

        const auto c = [](auto i) { return static_cast<std::byte>(i); };

        auto r = ::jaf::routing::core_router{ 0 };
        auto s1 = mock_socket{};
        auto s2 = mock_socket{};

        r.handle(s1, ::jaf::routing::register_hw_packet{ 1 });
        r.handle(s2, ::jaf::routing::register_hw_packet{ 2 });
        
        EXPECT_CALL(s1, send_data(_, _))
            .Times(1)
            .WillOnce(Invoke([&](const auto* b, auto s)
            {
                auto bf = std::vector<std::byte>(b, b + s);
                auto sr = ::jaf::serialize::streams::vector{ bf };

                auto id = ::jaf::routing::packet_id_t{};
                static_cast<::jaf::serialize::istream&>(sr) & id;
                ASSERT_EQ(id, 5);

                auto p = ::jaf::routing::broadcast_packet{};
                static_cast<::jaf::serialize::istream&>(sr) & p;
                
                const auto exps = ::jaf::routing::ip{1, 2, 3, 4};
                EXPECT_EQ(p.sender_, exps);
                const auto expd = std::vector<std::byte>{c(1), c(2), c(3), c(4), c(5)};
                EXPECT_EQ(p.data_, expd);
            }));
        
        EXPECT_CALL(s2, send_data(_, _))
            .Times(1)
            .WillOnce(Invoke([&](const auto* b, auto s)
            {
                auto bf = std::vector<std::byte>(b, b + s);
                auto sr = ::jaf::serialize::streams::vector{ bf };

                auto id = ::jaf::routing::packet_id_t{};
                static_cast<::jaf::serialize::istream&>(sr) & id;
                ASSERT_EQ(id, 5);

                auto p = ::jaf::routing::broadcast_packet{};
                static_cast<::jaf::serialize::istream&>(sr) & p;
                
                const auto exps = ::jaf::routing::ip{1, 2, 3, 4};
                EXPECT_EQ(p.sender_, exps);
                const auto expd = std::vector<std::byte>{c(1), c(2), c(3), c(4), c(5)};
                EXPECT_EQ(p.data_, expd);
            }));


        const auto d = {c(1), c(2), c(3), c(4), c(5)};
        const auto sender = ::jaf::routing::ip{1, 2, 3, 4};
        const auto p = ::jaf::routing::broadcast_packet{ sender, d };
        r.handle(s1, p);
    }

    TEST_F(core_router, send_publish)
    {
        using ::testing::_;
        using ::testing::Invoke;

        const auto c = [](auto i) { return static_cast<std::byte>(i); };

        auto s = mock_socket{};
        auto r = std::make_shared<::jaf::routing::core_router>(0);
        auto rl = std::make_shared<mock_router_listener>();
        r->subscribe(rl);

        EXPECT_CALL(*rl, handle(_, _, _))
            .Times(1)
            .WillOnce(Invoke([&](auto&, const auto& s, const auto& d)
            {
                const auto exps = ::jaf::routing::ip{0, 0, 0, 0};
                EXPECT_EQ(s, exps);
                const auto expd = std::vector<std::byte>{c(1), c(2), c(3), c(4), c(5)};
                EXPECT_EQ(d, expd);
            }));
                
        const auto d = {c(1), c(2), c(3), c(4), c(5)};
        const auto recv = ::jaf::routing::ip{0, 0, 0, 0};
        r->send(recv, d);
    }

    TEST_F(core_router, send_delegate)
    {
        using ::testing::_;
        using ::testing::Invoke;

        const auto c = [](auto i) { return static_cast<std::byte>(i); };

        auto r = ::jaf::routing::core_router{ 0 };
        auto s1 = mock_socket{};
        auto s2 = mock_socket{};

        r.handle(s1, ::jaf::routing::register_hw_packet{ 1 });
        r.handle(s2, ::jaf::routing::register_hw_packet{ 2 });
        
        EXPECT_CALL(s2, send_data(_, _))
            .Times(1)
            .WillOnce(Invoke([&](const auto* b, auto s)
            {
                auto bf = std::vector<std::byte>(b, b + s);
                auto sr = ::jaf::serialize::streams::vector{ bf };

                auto id = ::jaf::routing::packet_id_t{};
                static_cast<::jaf::serialize::istream&>(sr) & id;
                ASSERT_EQ(id, 4);

                auto p = ::jaf::routing::data_packet{};
                static_cast<::jaf::serialize::istream&>(sr) & p;
                
                const auto exps = ::jaf::routing::ip{0, 0, 0, 0};
                EXPECT_EQ(p.sender_, exps);
                const auto expr = ::jaf::routing::ip{201, 4, 99, 2};
                EXPECT_EQ(p.receiver_, expr);
                const auto expd = std::vector<std::byte>{c(1), c(2), c(3), c(4), c(5)};
                EXPECT_EQ(p.data_, expd);
            }));
        
        const auto d = {c(1), c(2), c(3), c(4), c(5)};
        const auto recv = ::jaf::routing::ip{201, 4, 99, 2};
        r.send(recv, d);
    }

    TEST_F(core_router, send_no_delegate)
    {
        using ::testing::_;
        using ::testing::Invoke;

        const auto c = [](auto i) { return static_cast<std::byte>(i); };

        auto r = ::jaf::routing::core_router{ 0 };
        auto s1 = mock_socket{};
        auto s2 = mock_socket{};

        r.handle(s1, ::jaf::routing::register_hw_packet{ 1 });
        r.handle(s2, ::jaf::routing::register_hw_packet{ 2 });
        
        const auto d = {c(1), c(2), c(3), c(4), c(5)};
        const auto recv = ::jaf::routing::ip{3, 5, 1, 2};
        EXPECT_THROW(r.send(recv, d), std::invalid_argument);
    }
    
    TEST_F(core_router, broadcast)
    {
        using ::testing::_;
        using ::testing::Invoke;

        const auto c = [](auto i) { return static_cast<std::byte>(i); };

        auto r = ::jaf::routing::core_router{ 0 };
        auto s1 = mock_socket{};
        auto s2 = mock_socket{};

        r.handle(s1, ::jaf::routing::register_hw_packet{ 1 });
        r.handle(s2, ::jaf::routing::register_hw_packet{ 2 });
        
        EXPECT_CALL(s1, send_data(_, _))
            .Times(1)
            .WillOnce(Invoke([&](const auto* b, auto s)
            {
                auto bf = std::vector<std::byte>(b, b + s);
                auto sr = ::jaf::serialize::streams::vector{ bf };

                auto id = ::jaf::routing::packet_id_t{};
                static_cast<::jaf::serialize::istream&>(sr) & id;
                ASSERT_EQ(id, 5);

                auto p = ::jaf::routing::broadcast_packet{};
                static_cast<::jaf::serialize::istream&>(sr) & p;
                
                const auto exps = ::jaf::routing::ip{0, 0, 0, 0};
                EXPECT_EQ(p.sender_, exps);
                const auto expd = std::vector<std::byte>{c(1), c(2), c(3), c(4), c(5)};
                EXPECT_EQ(p.data_, expd);
            }));
        
        EXPECT_CALL(s2, send_data(_, _))
            .Times(1)
            .WillOnce(Invoke([&](const auto* b, auto s)
            {
                auto bf = std::vector<std::byte>(b, b + s);
                auto sr = ::jaf::serialize::streams::vector{ bf };

                auto id = ::jaf::routing::packet_id_t{};
                static_cast<::jaf::serialize::istream&>(sr) & id;
                ASSERT_EQ(id, 5);

                auto p = ::jaf::routing::broadcast_packet{};
                static_cast<::jaf::serialize::istream&>(sr) & p;
                
                const auto exps = ::jaf::routing::ip{0, 0, 0, 0};
                EXPECT_EQ(p.sender_, exps);
                const auto expd = std::vector<std::byte>{c(1), c(2), c(3), c(4), c(5)};
                EXPECT_EQ(p.data_, expd);
            }));


        const auto d = {c(1), c(2), c(3), c(4), c(5)};
        r.broadcast(d);
    }
}
