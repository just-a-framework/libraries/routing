#include <routing/mask.hpp>

namespace jaf::routing::test::mask
{
    struct mask
        : ::jaf::testing::test_suite
    {
    };
    
    TEST_F(mask, split_middle)
    {
        const auto m = ::jaf::routing::mask{::jaf::routing::ip{0, 0, 0, 0}, ::jaf::routing::ip{255, 255, 255, 255}};
        const auto v = m.split(2);

        const auto m1 = ::jaf::routing::mask{::jaf::routing::ip{0, 0, 0, 0}, ::jaf::routing::ip{127, 255, 255, 255}};
        const auto m2 = ::jaf::routing::mask{::jaf::routing::ip{128, 0, 0, 0}, ::jaf::routing::ip{255, 255, 255, 255}};

        EXPECT_EQ(v.size(), 2);
        EXPECT_EQ(v.at(0), m1);
        EXPECT_EQ(v.at(1), m2);
    }
    
    TEST_F(mask, split_one)
    {
        const auto m = ::jaf::routing::mask{::jaf::routing::ip{0, 0, 0, 0}, ::jaf::routing::ip{255, 255, 255, 255}};
        const auto v = m.split(1);

        EXPECT_EQ(v.size(), 1);
        EXPECT_EQ(v.at(0), m);
    }

    TEST_F(mask, split_not_enough)
    {
        const auto m = ::jaf::routing::mask{::jaf::routing::ip{0, 0, 0, 0}, ::jaf::routing::ip{0, 0, 0, 3}};
        
        EXPECT_THROW(m.split(5), std::invalid_argument);
    }

    TEST_F(mask, split_barely_enough)
    {
        const auto m = ::jaf::routing::mask{::jaf::routing::ip{0, 0, 0, 0}, ::jaf::routing::ip{0, 0, 0, 3}};
        
        const auto m1 = ::jaf::routing::mask{::jaf::routing::ip{0, 0, 0, 0}, ::jaf::routing::ip{0, 0, 0, 0}};
        const auto m2 = ::jaf::routing::mask{::jaf::routing::ip{0, 0, 0, 1}, ::jaf::routing::ip{0, 0, 0, 1}};
        const auto m3 = ::jaf::routing::mask{::jaf::routing::ip{0, 0, 0, 2}, ::jaf::routing::ip{0, 0, 0, 2}};
        const auto m4 = ::jaf::routing::mask{::jaf::routing::ip{0, 0, 0, 3}, ::jaf::routing::ip{0, 0, 0, 3}};

        const auto v = m.split(4);
        
        EXPECT_EQ(v.size(), 4);
        EXPECT_EQ(v.at(0), m1);
        EXPECT_EQ(v.at(1), m2);
        EXPECT_EQ(v.at(2), m3);
        EXPECT_EQ(v.at(3), m4);
    }
}
