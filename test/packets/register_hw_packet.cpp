#include <routing/packets/register_hw_packet.hpp>

#include <serialize/streams/vector.hpp>

namespace jaf::routing::test::register_hw_packet
{
    struct register_hw_packet
        : ::jaf::testing::test_suite
    {
    };

    TEST_F(register_hw_packet, archive)
    {
        auto b = std::vector<std::byte>{};
        auto s = ::jaf::serialize::streams::vector{ b };
        
        const auto p1 = ::jaf::routing::register_hw_packet{ 3 };
        static_cast<::jaf::serialize::ostream&>(s) & p1;

        auto p2 = ::jaf::routing::register_hw_packet{};
        static_cast<::jaf::serialize::istream&>(s) & p2;

        EXPECT_EQ(p1.hwid_, p2.hwid_);
    }
}
