#pragma once

#include <routing/ip.hpp>
#include <routing/packets/base_packet.hpp>

namespace jaf::routing
{
    struct broadcast_packet
        : base_packet<5>
    {
        ip sender_;
        std::vector<std::byte> data_;

        explicit broadcast_packet();
        explicit broadcast_packet(ip sender, std::vector<std::byte> data);

        serialize::stream& archive(serialize::stream& s) final;
    };

    bool operator==(const broadcast_packet& a, const broadcast_packet& b);
}
