#include <routing/packets/broadcast_packet.hpp>

#include <serialize/vector.hpp>

namespace jaf::routing
{
    broadcast_packet::broadcast_packet() = default;

    broadcast_packet::broadcast_packet(ip sender, std::vector<std::byte> data)
        : sender_{ std::move(sender) }
        , data_{ std::move(data) }
    {
    }

    serialize::stream& broadcast_packet::archive(serialize::stream& s)
    {
        return (s & sender_ & data_);
    }

    bool operator==(const broadcast_packet& a, const broadcast_packet& b)
    {
        return a.sender_ == b.sender_ && a.data_ == b.data_;
    }
}
