#include <routing/socket.hpp>

#include <serialize/istream.hpp>
#include <serialize/ostream.hpp>
#include <serialize/streams/vector.hpp>

namespace jaf::routing
{
    socket_listener::~socket_listener() = default;

    void socket::send(const packet& p)
    {
        TRACE(logger);

        std::vector<std::byte> b{};
        serialize::streams::vector s{ b };
        auto id = p.identifier();
        static_cast<serialize::ostream&>(s) & id & p;

        send_data(b.data(), b.size());
    }

    void socket::deliver(const std::byte* b, size_t s)
    {
        TRACE(logger);

        std::vector<std::byte> buf{ b, b + s };
        serialize::streams::vector sr{ buf };

        packet_id_t id;
        static_cast<serialize::istream&>(sr) & id;

        if (id == register_hw_packet::id)
        {
            register_hw_packet p;
            static_cast<serialize::istream&>(sr) & p;

            auto handle = static_cast<void(socket_listener::*)(jaf::routing::socket&, const register_hw_packet&)>(&socket_listener::handle);
            publish(std::move(handle), p);
        }
        else if (id == unregister_hw_packet::id)
        {
            unregister_hw_packet p;
            static_cast<serialize::istream&>(sr) & p;

            auto handle = static_cast<void(socket_listener::*)(jaf::routing::socket&, const unregister_hw_packet&)>(&socket_listener::handle);
            publish(std::move(handle), p);
        }
        else if (id == address_config_packet::id)
        {
            address_config_packet p{};
            static_cast<serialize::istream&>(sr) & p;

            auto handle = static_cast<void(socket_listener::*)(jaf::routing::socket&, const address_config_packet&)>(&socket_listener::handle);
            publish(std::move(handle), p);
        }
        else if (id == failure_config_packet::id)
        {
            failure_config_packet p;
            static_cast<serialize::istream&>(sr) & p;

            auto handle = static_cast<void(socket_listener::*)(jaf::routing::socket&, const failure_config_packet&)>(&socket_listener::handle);
            publish(std::move(handle), p);
        }
        else if (id == data_packet::id)
        {
            data_packet p;
            static_cast<serialize::istream&>(sr) & p;

            auto handle = static_cast<void(socket_listener::*)(jaf::routing::socket&, const data_packet&)>(&socket_listener::handle);
            publish(std::move(handle), p);
        }
        else if (id == broadcast_packet::id)
        {
            broadcast_packet p;
            static_cast<serialize::istream&>(sr) & p;

            auto handle = static_cast<void(socket_listener::*)(jaf::routing::socket&, const broadcast_packet&)>(&socket_listener::handle);
            publish(std::move(handle), p);
        }
    }

    socket::~socket() = default;
}
