#include <routing/packets/unregister_hw_packet.hpp>

namespace jaf::routing
{
    unregister_hw_packet::unregister_hw_packet() = default;

    unregister_hw_packet::unregister_hw_packet(uint8_t hwid)
        : hwid_{ hwid }
    {
    }

    serialize::stream& unregister_hw_packet::archive(serialize::stream& s)
    {
        return (s & hwid_);
    }

    bool operator==(const unregister_hw_packet& a, const unregister_hw_packet& b)
    {
        return a.hwid_ == b.hwid_;
    }
}
