#include <routing/packets/data_packet.hpp>

#include <serialize/vector.hpp>

namespace jaf::routing
{
    data_packet::data_packet() = default;

    data_packet::data_packet(ip sender, ip receiver, std::vector<std::byte> data)
        : sender_{ std::move(sender) }
        , receiver_{ std::move(receiver) }
        , data_{ std::move(data) }
    {
    }

    serialize::stream& data_packet::archive(serialize::stream& s)
    {
        return (s & sender_ & receiver_ & data_);
    }

    bool operator==(const data_packet& a, const data_packet& b)
    {
        return a.sender_ == b.sender_ && a.receiver_ == b.receiver_ && a.data_ == b.data_;
    }
}
