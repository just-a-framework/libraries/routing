#pragma once

#include <routing/logger.hpp>
#include <signal/publisher.hpp>
#include <routing/packets/register_hw_packet.hpp>
#include <routing/packets/unregister_hw_packet.hpp>
#include <routing/packets/address_config_packet.hpp>
#include <routing/packets/failure_config_packet.hpp>
#include <routing/packets/data_packet.hpp>
#include <routing/packets/broadcast_packet.hpp>

namespace jaf::routing
{
    struct socket;

    struct socket_listener
    {
        virtual void handle(socket&, const register_hw_packet&) = 0;
        virtual void handle(socket&, const unregister_hw_packet&) = 0;
        virtual void handle(socket&, const address_config_packet&) = 0;
        virtual void handle(socket&, const failure_config_packet&) = 0;
        virtual void handle(socket&, const data_packet&) = 0;
        virtual void handle(socket&, const broadcast_packet&) = 0;

        virtual ~socket_listener();
    };

    struct socket
        : signal::publisher<socket, socket_listener>
    {
        virtual void send_data(const std::byte* b, size_t s) = 0;
        void deliver(const std::byte* b, size_t s);

        void send(const packet& p);

        virtual ~socket();
    };
}
