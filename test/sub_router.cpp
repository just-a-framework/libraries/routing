#include <routing/sub_router.hpp>
#include <serialize/streams/vector.hpp>

namespace jaf::routing::test::sub_router
{
    struct sub_router
        : ::jaf::testing::test_suite
    {
    };

    struct mock_socket
        : ::jaf::routing::socket
    {
        MOCK_METHOD(void, send_data, (const std::byte*, size_t), ());
    };

    struct mock_router_listener
        : ::jaf::routing::router_listener
    {
        MOCK_METHOD(void, handle, (::jaf::routing::router&, const ::jaf::routing::ip&, const std::vector<std::byte>&), ());
    };

    void configure_address(std::shared_ptr<::jaf::routing::socket> s, const ::jaf::routing::mask& m)
    {
        auto bf = std::vector<std::byte>{};
        auto sr = ::jaf::serialize::streams::vector{ bf };

        const auto p = ::jaf::routing::address_config_packet{m};
        const auto id = p.identifier();
        static_cast<::jaf::serialize::ostream&>(sr) & id & p;

        s->deliver(bf.data(), bf.size());
    }
    
    void send_data(std::shared_ptr<::jaf::routing::socket> s, const ::jaf::routing::packet& p)
    {
        auto bf = std::vector<std::byte>{};
        auto sr = ::jaf::serialize::streams::vector{ bf };

        const auto id = p.identifier();
        static_cast<::jaf::serialize::ostream&>(sr) & id & p;

        s->deliver(bf.data(), bf.size());
    }

    TEST_F(sub_router, automatic_register)
    {
        using ::testing::_;
        using ::testing::Invoke;

        auto gl = std::make_shared<mock_socket>();

        EXPECT_CALL(*gl, send_data(_, _))
            .Times(1)
            .WillOnce(Invoke([&](const auto* b, auto s)
            {
                auto bf = std::vector<std::byte>(b, b + s);
                auto sr = ::jaf::serialize::streams::vector{ bf };

                auto id = ::jaf::routing::packet_id_t{};
                static_cast<::jaf::serialize::istream&>(sr) & id;
                ASSERT_EQ(id, 0);

                auto p = ::jaf::routing::register_hw_packet{};
                static_cast<::jaf::serialize::istream&>(sr) & p;

                EXPECT_EQ(p.hwid_, 2);

                configure_address(gl, {{1, 2, 3, 4}, {5, 6, 7, 8}});
            }));
        
        auto r = std::make_shared<::jaf::routing::sub_router>(2, gl);
        
        EXPECT_CALL(*gl, send_data(_, _))
            .Times(1)
            .WillOnce(Invoke([&](const auto* b, auto s)
            {
                auto bf = std::vector<std::byte>(b, b + s);
                auto sr = ::jaf::serialize::streams::vector{ bf };

                auto id = ::jaf::routing::packet_id_t{};
                static_cast<::jaf::serialize::istream&>(sr) & id;
                ASSERT_EQ(id, 1);

                auto p = ::jaf::routing::register_hw_packet{};
                static_cast<::jaf::serialize::istream&>(sr) & p;

                EXPECT_EQ(p.hwid_, 2);
            }));
        
        r.reset();
    }

     TEST_F(sub_router, register_failure)
    {
        using ::testing::_;
        using ::testing::Invoke;

        auto gl = std::make_shared<mock_socket>();

        EXPECT_CALL(*gl, send_data(_, _))
            .Times(1)
            .WillOnce(Invoke([&](const auto* b, auto s)
            {
                {
                    auto bf = std::vector<std::byte>(b, b + s);
                    auto sr = ::jaf::serialize::streams::vector{ bf };

                    auto id = ::jaf::routing::packet_id_t{};
                    static_cast<::jaf::serialize::istream&>(sr) & id;
                    ASSERT_EQ(id, 0);

                    auto p = ::jaf::routing::register_hw_packet{};
                    static_cast<::jaf::serialize::istream&>(sr) & p;

                    EXPECT_EQ(p.hwid_, 2);
                }

                {
                    auto bf = std::vector<std::byte>{};
                    auto sr = ::jaf::serialize::streams::vector{ bf };

                    const auto p = ::jaf::routing::failure_config_packet{};
                    const auto id = p.identifier();
                    static_cast<::jaf::serialize::ostream&>(sr) & id & p;

                    gl->deliver(bf.data(), bf.size());
                }
            }));
        
        EXPECT_THROW(auto r = std::make_shared<::jaf::routing::sub_router>(2, gl), std::invalid_argument);
    }

    TEST_F(sub_router, handle_data_publish)
    {
        using ::testing::_;
        using ::testing::Invoke;

        const auto c = [](auto i) { return static_cast<std::byte>(i); };

        auto s = mock_socket{};
        auto gl = std::make_shared<mock_socket>();
        auto r = std::make_shared<::jaf::routing::sub_router>(0, gl);
        auto rl = std::make_shared<mock_router_listener>();
        r->subscribe(rl);
        configure_address(gl, {{1, 2, 3, 4}, {5, 6, 7, 8}});

        EXPECT_CALL(*rl, handle(_, _, _))
            .Times(1)
            .WillOnce(Invoke([&](auto&, const auto& s, const auto& d)
            {
                const auto exps = ::jaf::routing::ip{3, 2, 4, 6};
                EXPECT_EQ(s, exps);
                const auto expd = std::vector<std::byte>{c(1), c(2), c(3), c(4), c(5)};
                EXPECT_EQ(d, expd);
            }));
        
        const auto p = ::jaf::routing::data_packet{{3, 2, 4, 6}, {1, 2, 3, 4}, {c(1), c(2), c(3), c(4), c(5)}};
        r->handle(s, p);
    }

    TEST_F(sub_router, handle_data_delegate)
    {
        using ::testing::_;
        using ::testing::Invoke;

        const auto c = [](auto i) { return static_cast<std::byte>(i); };

        auto gl = std::make_shared<mock_socket>();
        auto r = ::jaf::routing::sub_router{ 0, gl };
        auto s1 = mock_socket{};
        auto s2 = mock_socket{};
        configure_address(gl, {{1, 2, 3, 4}, {5, 6, 7, 8}});

        r.handle(s1, ::jaf::routing::register_hw_packet{ 1 });
        r.handle(s2, ::jaf::routing::register_hw_packet{ 2 });
        
        EXPECT_CALL(s2, send_data(_, _))
            .Times(1)
            .WillOnce(Invoke([&](const auto* b, auto s)
            {
                auto bf = std::vector<std::byte>(b, b + s);
                auto sr = ::jaf::serialize::streams::vector{ bf };

                auto id = ::jaf::routing::packet_id_t{};
                static_cast<::jaf::serialize::istream&>(sr) & id;
                ASSERT_EQ(id, 4);

                auto p = ::jaf::routing::data_packet{};
                static_cast<::jaf::serialize::istream&>(sr) & p;
                
                const auto exps = ::jaf::routing::ip{1, 2, 3, 4};
                EXPECT_EQ(p.sender_, exps);
                const auto expr = ::jaf::routing::ip{4, 5, 7, 5};
                EXPECT_EQ(p.receiver_, expr);
                const auto expd = std::vector<std::byte>{c(1), c(2), c(3), c(4), c(5)};
                EXPECT_EQ(p.data_, expd);
            }));
        
        const auto p = ::jaf::routing::data_packet{{1, 2, 3, 4}, {4, 5, 7, 5}, {c(1), c(2), c(3), c(4), c(5)}};
        r.handle(s1, p);
    }

    TEST_F(sub_router, handle_data_no_delegate)
    {
        using ::testing::_;
        using ::testing::Invoke;

        const auto c = [](auto i) { return static_cast<std::byte>(i); };

        auto gl = std::make_shared<mock_socket>();
        auto r = ::jaf::routing::sub_router{ 0, gl };
        auto s1 = mock_socket{};
        auto s2 = mock_socket{};
        configure_address(gl, {{1, 2, 3, 4}, {5, 6, 7, 8}});

        r.handle(s1, ::jaf::routing::register_hw_packet{ 1 });
        r.handle(s2, ::jaf::routing::register_hw_packet{ 2 });
        
        const auto p = ::jaf::routing::data_packet{{1, 2, 3, 4}, {4, 5, 5, 5}, {c(1), c(2), c(3), c(4), c(5)}};
        EXPECT_THROW(r.handle(s1, p), std::invalid_argument);
    }

    TEST_F(sub_router, handle_data_outside)
    {
        using ::testing::_;
        using ::testing::Invoke;

        const auto c = [](auto i) { return static_cast<std::byte>(i); };

        auto gl = std::make_shared<mock_socket>();
        auto r = std::make_shared<::jaf::routing::sub_router>(0, gl);
        auto s1 = mock_socket{};
        auto s2 = mock_socket{};
        configure_address(gl, {{1, 2, 3, 4}, {5, 6, 7, 8}});

        r->handle(s1, ::jaf::routing::register_hw_packet{ 1 });
        r->handle(s2, ::jaf::routing::register_hw_packet{ 2 });
            
        EXPECT_CALL(*gl, send_data(_, _))
            .Times(1)
            .WillOnce(Invoke([&](const auto* b, auto s)
            {
                auto bf = std::vector<std::byte>(b, b + s);
                auto sr = ::jaf::serialize::streams::vector{ bf };

                auto id = ::jaf::routing::packet_id_t{};
                static_cast<::jaf::serialize::istream&>(sr) & id;
                ASSERT_EQ(id, 4);

                auto p = ::jaf::routing::data_packet{};
                static_cast<::jaf::serialize::istream&>(sr) & p;
                
                const auto exps = ::jaf::routing::ip{1, 2, 3, 4};
                EXPECT_EQ(p.sender_, exps);
                const auto expr = ::jaf::routing::ip{6, 7, 8, 9};
                EXPECT_EQ(p.receiver_, expr);
                const auto expd = std::vector<std::byte>{c(1), c(2), c(3), c(4), c(5)};
                EXPECT_EQ(p.data_, expd);
            }));
        
        const auto p = ::jaf::routing::data_packet{{1, 2, 3, 4}, {6, 7, 8, 9}, {c(1), c(2), c(3), c(4), c(5)}};
        r->handle(s1, p);

        EXPECT_CALL(*gl, send_data(_, _))
            .Times(1);
        r.reset();
    }

    TEST_F(sub_router, handle_broadcast)
    {
        using ::testing::_;
        using ::testing::Invoke;

        const auto c = [](auto i) { return static_cast<std::byte>(i); };

        auto gl = std::make_shared<mock_socket>();
        auto r = std::make_shared<::jaf::routing::sub_router>(0, gl);
        auto s1 = mock_socket{};
        auto s2 = mock_socket{};
        configure_address(gl, {{1, 2, 3, 4}, {5, 6, 7, 8}});

        r->handle(s1, ::jaf::routing::register_hw_packet{ 1 });
        r->handle(s2, ::jaf::routing::register_hw_packet{ 2 });
            
        EXPECT_CALL(*gl, send_data(_, _))
            .Times(1)
            .WillOnce(Invoke([&](const auto* b, auto s)
            {
                auto bf = std::vector<std::byte>(b, b + s);
                auto sr = ::jaf::serialize::streams::vector{ bf };

                auto id = ::jaf::routing::packet_id_t{};
                static_cast<::jaf::serialize::istream&>(sr) & id;
                ASSERT_EQ(id, 5);

                auto p = ::jaf::routing::broadcast_packet{};
                static_cast<::jaf::serialize::istream&>(sr) & p;
                
                const auto exps = ::jaf::routing::ip{3, 5, 1, 2};
                EXPECT_EQ(p.sender_, exps);
                const auto expd = std::vector<std::byte>{c(1), c(2), c(3), c(4), c(5)};
                EXPECT_EQ(p.data_, expd);
            }));
        
        const auto d = {c(1), c(2), c(3), c(4), c(5)};
        const auto sender = ::jaf::routing::ip{3, 5, 1, 2};        
        const auto p = ::jaf::routing::broadcast_packet{ sender, d };
        r->handle(s1, p);

        EXPECT_CALL(*gl, send_data(_, _))
            .Times(1);
        r.reset();
    }

    TEST_F(sub_router, send_publish)
    {
        using ::testing::_;
        using ::testing::Invoke;

        const auto c = [](auto i) { return static_cast<std::byte>(i); };

        auto gl = std::make_shared<mock_socket>();
        auto r = std::make_shared<::jaf::routing::sub_router>(0, gl);
        auto rl = std::make_shared<mock_router_listener>();
        r->subscribe(rl);
        configure_address(gl, {{1, 2, 3, 4}, {5, 6, 7, 8}});

        EXPECT_CALL(*rl, handle(_, _, _))
            .Times(1)
            .WillOnce(Invoke([&](auto&, const auto& s, const auto& d)
            {
                const auto exps = ::jaf::routing::ip{1, 2, 3, 4};
                EXPECT_EQ(s, exps);
                const auto expd = std::vector<std::byte>{c(1), c(2), c(3), c(4), c(5)};
                EXPECT_EQ(d, expd);
            }));
                        
        const auto d = {c(1), c(2), c(3), c(4), c(5)};
        const auto recv = ::jaf::routing::ip{1, 2, 3, 4};
        r->send(recv, d);
    }

    TEST_F(sub_router, send_delegate)
    {
        using ::testing::_;
        using ::testing::Invoke;

        const auto c = [](auto i) { return static_cast<std::byte>(i); };

        auto gl = std::make_shared<mock_socket>();
        auto r = ::jaf::routing::sub_router{ 0, gl };
        auto s1 = mock_socket{};
        auto s2 = mock_socket{};
        configure_address(gl, {{1, 2, 3, 4}, {5, 6, 7, 8}});

        r.handle(s1, ::jaf::routing::register_hw_packet{ 1 });
        r.handle(s2, ::jaf::routing::register_hw_packet{ 2 });
        
        EXPECT_CALL(s2, send_data(_, _))
            .Times(1)
            .WillOnce(Invoke([&](const auto* b, auto s)
            {
                auto bf = std::vector<std::byte>(b, b + s);
                auto sr = ::jaf::serialize::streams::vector{ bf };

                auto id = ::jaf::routing::packet_id_t{};
                static_cast<::jaf::serialize::istream&>(sr) & id;
                ASSERT_EQ(id, 4);

                auto p = ::jaf::routing::data_packet{};
                static_cast<::jaf::serialize::istream&>(sr) & p;
                
                const auto exps = ::jaf::routing::ip{1, 2, 3, 4};
                EXPECT_EQ(p.sender_, exps);
                const auto expr = ::jaf::routing::ip{4, 5, 7, 5};
                EXPECT_EQ(p.receiver_, expr);
                const auto expd = std::vector<std::byte>{c(1), c(2), c(3), c(4), c(5)};
                EXPECT_EQ(p.data_, expd);
            }));
                
        const auto d = {c(1), c(2), c(3), c(4), c(5)};
        const auto recv = ::jaf::routing::ip{4, 5, 7, 5};
        r.send(recv, d);
    }

    TEST_F(sub_router, send_no_delegate)
    {
        using ::testing::_;
        using ::testing::Invoke;

        const auto c = [](auto i) { return static_cast<std::byte>(i); };

        auto gl = std::make_shared<mock_socket>();
        auto r = ::jaf::routing::sub_router{ 0, gl };
        auto s1 = mock_socket{};
        auto s2 = mock_socket{};
        configure_address(gl, {{1, 2, 3, 4}, {5, 6, 7, 8}});

        r.handle(s1, ::jaf::routing::register_hw_packet{ 1 });
        r.handle(s2, ::jaf::routing::register_hw_packet{ 2 });
        
        const auto d = {c(1), c(2), c(3), c(4), c(5)};
        const auto recv = ::jaf::routing::ip{4, 5, 5, 5};
        EXPECT_THROW(r.send(recv, d), std::invalid_argument);
    }

    TEST_F(sub_router, send_outside)
    {
        using ::testing::_;
        using ::testing::Invoke;

        const auto c = [](auto i) { return static_cast<std::byte>(i); };

        auto gl = std::make_shared<mock_socket>();
        auto r = std::make_shared<::jaf::routing::sub_router>( 0, gl );
        auto s1 = mock_socket{};
        auto s2 = mock_socket{};
        configure_address(gl, {{1, 2, 3, 4}, {5, 6, 7, 8}});

        r->handle(s1, ::jaf::routing::register_hw_packet{ 1 });
        r->handle(s2, ::jaf::routing::register_hw_packet{ 2 });
            
        EXPECT_CALL(*gl, send_data(_, _))
            .Times(1)
            .WillOnce(Invoke([&](const auto* b, auto s)
            {
                auto bf = std::vector<std::byte>(b, b + s);
                auto sr = ::jaf::serialize::streams::vector{ bf };

                auto id = ::jaf::routing::packet_id_t{};
                static_cast<::jaf::serialize::istream&>(sr) & id;
                ASSERT_EQ(id, 4);

                auto p = ::jaf::routing::data_packet{};
                static_cast<::jaf::serialize::istream&>(sr) & p;
                
                const auto exps = ::jaf::routing::ip{1, 2, 3, 4};
                EXPECT_EQ(p.sender_, exps);
                const auto expr = ::jaf::routing::ip{6, 7, 8, 9};
                EXPECT_EQ(p.receiver_, expr);
                const auto expd = std::vector<std::byte>{c(1), c(2), c(3), c(4), c(5)};
                EXPECT_EQ(p.data_, expd);
            }));
        
        const auto d = {c(1), c(2), c(3), c(4), c(5)};
        const auto recv = ::jaf::routing::ip{6, 7, 8, 9};
        r->send(recv, d);

        EXPECT_CALL(*gl, send_data(_, _))
            .Times(1);
        r.reset();
    }

    TEST_F(sub_router, broadcast)
    {
        using ::testing::_;
        using ::testing::Invoke;

        const auto c = [](auto i) { return static_cast<std::byte>(i); };

        auto gl = std::make_shared<mock_socket>();
        auto r = std::make_shared<::jaf::routing::sub_router>( 0, gl );
        auto s1 = mock_socket{};
        auto s2 = mock_socket{};
        configure_address(gl, {{1, 2, 3, 4}, {5, 6, 7, 8}});

        r->handle(s1, ::jaf::routing::register_hw_packet{ 1 });
        r->handle(s2, ::jaf::routing::register_hw_packet{ 2 });
            
        EXPECT_CALL(*gl, send_data(_, _))
            .Times(1)
            .WillOnce(Invoke([&](const auto* b, auto s)
            {
                auto bf = std::vector<std::byte>(b, b + s);
                auto sr = ::jaf::serialize::streams::vector{ bf };

                auto id = ::jaf::routing::packet_id_t{};
                static_cast<::jaf::serialize::istream&>(sr) & id;
                ASSERT_EQ(id, 5);

                auto p = ::jaf::routing::broadcast_packet{};
                static_cast<::jaf::serialize::istream&>(sr) & p;
                
                const auto exps = ::jaf::routing::ip{1, 2, 3, 4};
                EXPECT_EQ(p.sender_, exps);
                const auto expd = std::vector<std::byte>{c(1), c(2), c(3), c(4), c(5)};
                EXPECT_EQ(p.data_, expd);
            }));
        
        const auto d = {c(1), c(2), c(3), c(4), c(5)};
        r->broadcast(d);

        EXPECT_CALL(*gl, send_data(_, _))
            .Times(1);
        r.reset();
    }

    TEST_F(sub_router, gateway_handle_data_publish)
    {
        using ::testing::_;
        using ::testing::Invoke;

        const auto c = [](auto i) { return static_cast<std::byte>(i); };

        auto s = mock_socket{};
        auto gl = std::make_shared<mock_socket>();
        auto r = std::make_shared<::jaf::routing::sub_router>(0, gl);
        auto rl = std::make_shared<mock_router_listener>();
        r->subscribe(rl);
        configure_address(gl, {{1, 2, 3, 4}, {5, 6, 7, 8}});

        EXPECT_CALL(*rl, handle(_, _, _))
            .Times(1)
            .WillOnce(Invoke([&](auto&, const auto& s, const auto& d)
            {
                const auto exps = ::jaf::routing::ip{3, 2, 4, 6};
                EXPECT_EQ(s, exps);
                const auto expd = std::vector<std::byte>{c(1), c(2), c(3), c(4), c(5)};
                EXPECT_EQ(d, expd);
            }));
        
        const auto p = ::jaf::routing::data_packet{{3, 2, 4, 6}, {1, 2, 3, 4}, {c(1), c(2), c(3), c(4), c(5)}};
        send_data(gl, p);
    }

    TEST_F(sub_router, gateway_handle_data_delegate)
    {
        using ::testing::_;
        using ::testing::Invoke;

        const auto c = [](auto i) { return static_cast<std::byte>(i); };

        auto gl = std::make_shared<mock_socket>();
        auto r = ::jaf::routing::sub_router{ 0, gl };
        auto s1 = mock_socket{};
        auto s2 = mock_socket{};
        configure_address(gl, {{1, 2, 3, 4}, {5, 6, 7, 8}});

        r.handle(s1, ::jaf::routing::register_hw_packet{ 1 });
        r.handle(s2, ::jaf::routing::register_hw_packet{ 2 });
        
        EXPECT_CALL(s2, send_data(_, _))
            .Times(1)
            .WillOnce(Invoke([&](const auto* b, auto s)
            {
                auto bf = std::vector<std::byte>(b, b + s);
                auto sr = ::jaf::serialize::streams::vector{ bf };

                auto id = ::jaf::routing::packet_id_t{};
                static_cast<::jaf::serialize::istream&>(sr) & id;
                ASSERT_EQ(id, 4);

                auto p = ::jaf::routing::data_packet{};
                static_cast<::jaf::serialize::istream&>(sr) & p;
                
                const auto exps = ::jaf::routing::ip{1, 2, 3, 4};
                EXPECT_EQ(p.sender_, exps);
                const auto expr = ::jaf::routing::ip{4, 5, 7, 5};
                EXPECT_EQ(p.receiver_, expr);
                const auto expd = std::vector<std::byte>{c(1), c(2), c(3), c(4), c(5)};
                EXPECT_EQ(p.data_, expd);
            }));
        
        const auto p = ::jaf::routing::data_packet{{1, 2, 3, 4}, {4, 5, 7, 5}, {c(1), c(2), c(3), c(4), c(5)}};
        send_data(gl, p);
    }

    TEST_F(sub_router, gateway_handle_data_no_delegate)
    {
        using ::testing::_;
        using ::testing::Invoke;

        const auto c = [](auto i) { return static_cast<std::byte>(i); };

        auto gl = std::make_shared<mock_socket>();
        auto r = ::jaf::routing::sub_router{ 0, gl };
        auto s1 = mock_socket{};
        auto s2 = mock_socket{};
        configure_address(gl, {{1, 2, 3, 4}, {5, 6, 7, 8}});

        r.handle(s1, ::jaf::routing::register_hw_packet{ 1 });
        r.handle(s2, ::jaf::routing::register_hw_packet{ 2 });
        
        const auto p = ::jaf::routing::data_packet{{1, 2, 3, 4}, {4, 5, 5, 5}, {c(1), c(2), c(3), c(4), c(5)}};
        EXPECT_THROW(send_data(gl, p), std::invalid_argument);
    }

    TEST_F(sub_router, gateway_handle_data_outside)
    {
        using ::testing::_;
        using ::testing::Invoke;

        const auto c = [](auto i) { return static_cast<std::byte>(i); };

        auto gl = std::make_shared<mock_socket>();
        auto r = ::jaf::routing::sub_router{ 0, gl };
        auto s1 = mock_socket{};
        auto s2 = mock_socket{};
        configure_address(gl, {{1, 2, 3, 4}, {5, 6, 7, 8}});

        r.handle(s1, ::jaf::routing::register_hw_packet{ 1 });
        r.handle(s2, ::jaf::routing::register_hw_packet{ 2 });
        
        const auto p = ::jaf::routing::data_packet{{1, 2, 3, 4}, {6, 7, 8, 9}, {c(1), c(2), c(3), c(4), c(5)}};
        EXPECT_THROW(send_data(gl, p), std::invalid_argument);
    }

    TEST_F(sub_router, gateway_handle_broadcast)
    {
        using ::testing::_;
        using ::testing::Invoke;

        const auto c = [](auto i) { return static_cast<std::byte>(i); };

        auto gl = std::make_shared<mock_socket>();
        auto r = ::jaf::routing::sub_router{ 0, gl };
        auto s1 = mock_socket{};
        auto s2 = mock_socket{};
        configure_address(gl, {{1, 2, 3, 4}, {5, 6, 7, 8}});

        r.handle(s1, ::jaf::routing::register_hw_packet{ 1 });
        r.handle(s2, ::jaf::routing::register_hw_packet{ 2 });
        
        EXPECT_CALL(s1, send_data(_, _))
            .Times(1)
            .WillOnce(Invoke([&](const auto* b, auto s)
            {
                auto bf = std::vector<std::byte>(b, b + s);
                auto sr = ::jaf::serialize::streams::vector{ bf };

                auto id = ::jaf::routing::packet_id_t{};
                static_cast<::jaf::serialize::istream&>(sr) & id;
                ASSERT_EQ(id, 5);

                auto p = ::jaf::routing::broadcast_packet{};
                static_cast<::jaf::serialize::istream&>(sr) & p;
                
                const auto exps = ::jaf::routing::ip{3, 5, 1, 2};
                EXPECT_EQ(p.sender_, exps);
                const auto expd = std::vector<std::byte>{c(1), c(2), c(3), c(4), c(5)};
                EXPECT_EQ(p.data_, expd);
            }));
        
        EXPECT_CALL(s2, send_data(_, _))
            .Times(1)
            .WillOnce(Invoke([&](const auto* b, auto s)
            {
                auto bf = std::vector<std::byte>(b, b + s);
                auto sr = ::jaf::serialize::streams::vector{ bf };

                auto id = ::jaf::routing::packet_id_t{};
                static_cast<::jaf::serialize::istream&>(sr) & id;
                ASSERT_EQ(id, 5);

                auto p = ::jaf::routing::broadcast_packet{};
                static_cast<::jaf::serialize::istream&>(sr) & p;
                
                const auto exps = ::jaf::routing::ip{3, 5, 1, 2};
                EXPECT_EQ(p.sender_, exps);
                const auto expd = std::vector<std::byte>{c(1), c(2), c(3), c(4), c(5)};
                EXPECT_EQ(p.data_, expd);
            }));


        const auto d = {c(1), c(2), c(3), c(4), c(5)};
        const auto sender = ::jaf::routing::ip{3, 5, 1, 2};        
        const auto p = ::jaf::routing::broadcast_packet{ sender, d };
        send_data(gl, p);
    }
}
