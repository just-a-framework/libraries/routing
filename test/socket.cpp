#include <routing/socket.hpp>

#include <serialize/streams/vector.hpp>

namespace jaf::routing::test::socket
{
    struct socket
        : ::jaf::testing::test_suite
    {
    };

    struct mock_socket
        : ::jaf::routing::socket
    {
        MOCK_METHOD(void, send_data, (const std::byte*, size_t), ());
    };

    struct mock_socket_listener
        : ::jaf::routing::socket_listener
    {
        MOCK_METHOD(void, handle, (::jaf::routing::socket&, const ::jaf::routing::register_hw_packet&), ());
        MOCK_METHOD(void, handle, (::jaf::routing::socket&, const ::jaf::routing::unregister_hw_packet&), ());
        MOCK_METHOD(void, handle, (::jaf::routing::socket&, const ::jaf::routing::address_config_packet&), ());
        MOCK_METHOD(void, handle, (::jaf::routing::socket&, const ::jaf::routing::failure_config_packet&), ());
        MOCK_METHOD(void, handle, (::jaf::routing::socket&, const ::jaf::routing::data_packet&), ());
        MOCK_METHOD(void, handle, (::jaf::routing::socket&, const ::jaf::routing::broadcast_packet&), ());
    };

    TEST_F(socket, deliver_register_hw_packet)
    {
        using ::testing::_;

        auto b = std::vector<std::byte>{};
        auto sr = ::jaf::serialize::streams::vector{ b };

        const auto p = ::jaf::routing::register_hw_packet{ 2 };
        auto id = p.identifier();
        static_cast<::jaf::serialize::ostream&>(sr) & id & p;

        auto soc_lis = std::make_shared<mock_socket_listener>();
        auto soc = std::make_shared<mock_socket>();
        soc->subscribe(soc_lis);

        EXPECT_CALL(*soc_lis, handle(_, p))
            .Times(1);

        soc->deliver(b.data(), b.size());
    }

    TEST_F(socket, deliver_unregister_hw_packet)
    {
        using ::testing::_;

        auto b = std::vector<std::byte>{};
        auto sr = ::jaf::serialize::streams::vector{ b };

        const auto p = ::jaf::routing::unregister_hw_packet{ 3 };
        auto id = p.identifier();
        static_cast<::jaf::serialize::ostream&>(sr) & id & p;

        auto soc_lis = std::make_shared<mock_socket_listener>();
        auto soc = std::make_shared<mock_socket>();
        soc->subscribe(soc_lis);

        EXPECT_CALL(*soc_lis, handle(_, p))
            .Times(1);

        soc->deliver(b.data(), b.size());
    }

    TEST_F(socket, deliver_address_config_packet)
    {
        using ::testing::_;

        auto b = std::vector<std::byte>{};
        auto sr = ::jaf::serialize::streams::vector{ b };

        const auto p = ::jaf::routing::address_config_packet{ { { 1, 2, 3, 4 }, { 5, 6, 7, 8 } } };
        auto id = p.identifier();
        static_cast<::jaf::serialize::ostream&>(sr) & id & p;

        auto soc_lis = std::make_shared<mock_socket_listener>();
        auto soc = std::make_shared<mock_socket>();
        soc->subscribe(soc_lis);

        EXPECT_CALL(*soc_lis, handle(_, p))
            .Times(1);

        soc->deliver(b.data(), b.size());
    }

    TEST_F(socket, deliver_failure_config_packet)
    {
        using ::testing::_;

        auto b = std::vector<std::byte>{};
        auto sr = ::jaf::serialize::streams::vector{ b };

        const auto p = ::jaf::routing::failure_config_packet{};
        auto id = p.identifier();
        static_cast<::jaf::serialize::ostream&>(sr) & id & p;

        auto soc_lis = std::make_shared<mock_socket_listener>();
        auto soc = std::make_shared<mock_socket>();
        soc->subscribe(soc_lis);

        EXPECT_CALL(*soc_lis, handle(_, p))
            .Times(1);

        soc->deliver(b.data(), b.size());
    }

    TEST_F(socket, deliver_data_packet)
    {
        using ::testing::_;

        auto b = std::vector<std::byte>{};
        auto sr = ::jaf::serialize::streams::vector{ b };

        const auto c = [](const unsigned char i) -> std::byte { return static_cast<std::byte>(i); };

        const auto data = std::vector<std::byte>{ c(0), c(6), c(0), c(1), c(9), c(1) };
        const auto p = ::jaf::routing::data_packet{ {1, 2, 3, 4}, {5, 6, 7, 8}, data };
        auto id = p.identifier();
        static_cast<::jaf::serialize::ostream&>(sr) & id & p;

        auto soc_lis = std::make_shared<mock_socket_listener>();
        auto soc = std::make_shared<mock_socket>();
        soc->subscribe(soc_lis);

        EXPECT_CALL(*soc_lis, handle(_, p))
            .Times(1);

        soc->deliver(b.data(), b.size());
    }

    TEST_F(socket, deliver_broadcast_packet)
    {
        using ::testing::_;

        auto b = std::vector<std::byte>{};
        auto sr = ::jaf::serialize::streams::vector{ b };

        const auto c = [](const unsigned char i) -> std::byte { return static_cast<std::byte>(i); };

        const auto data = std::vector<std::byte>{ c(0), c(6), c(0), c(1), c(9), c(1) };
        const auto p = ::jaf::routing::broadcast_packet{ {5, 6, 7, 8}, data };
        auto id = p.identifier();
        static_cast<::jaf::serialize::ostream&>(sr) & id & p;

        auto soc_lis = std::make_shared<mock_socket_listener>();
        auto soc = std::make_shared<mock_socket>();
        soc->subscribe(soc_lis);

        EXPECT_CALL(*soc_lis, handle(_, p))
            .Times(1);

        soc->deliver(b.data(), b.size());
    }

    TEST_F(socket, send_data)
    {
        using ::testing::_;
        using ::testing::Invoke;

        auto soc = mock_socket{};

        const auto checker = [](const auto* b, auto s)
        {
            EXPECT_EQ(s, 1);
            EXPECT_EQ(std::to_integer<int>(b[0]), 3);
        };

        EXPECT_CALL(soc, send_data(_, _))
            .Times(1)
            .WillOnce(Invoke(checker));

        soc.send(::jaf::routing::failure_config_packet{});
    }
}
