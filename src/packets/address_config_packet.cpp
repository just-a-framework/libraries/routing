#include <routing/packets/address_config_packet.hpp>

namespace jaf::routing
{
    address_config_packet::address_config_packet() = default;

    address_config_packet::address_config_packet(mask m)
        : m_{ std::move(m) }
    {
    }

    serialize::stream& address_config_packet::archive(serialize::stream& s)
    {
        return (s & m_);
    }

    bool operator==(const address_config_packet& a, const address_config_packet& b)
    {
        return a.m_ == b.m_;
    }
}
