#include <routing/core_router.hpp>

#include <routing/logger.hpp>

namespace jaf::routing
{
    core_router::core_router(uint8_t hwid, uint32_t max_clients)
        : base_router{ std::move(hwid), max_clients }
    {
        TRACE(logger);
        configure_subnetworks({{0, 0, 0, 0}, {255, 255, 255, 255}});
    }

    void core_router::handle(socket& s, const data_packet& p)
    {
        if(!handle_internal(s, p))
        {
            throw std::invalid_argument{ "ip???" };
        }
    }

    void core_router::handle(socket& s, const broadcast_packet& p)
    {
        broadcast_internal(p);
    }

    void core_router::send(const ip& receiver, const std::vector<std::byte>& data)
    {
        if(!send_internal(receiver, data))
        {
            throw std::invalid_argument{ "ip???" };
        }
    }

    void core_router::broadcast(const std::vector<std::byte>& data)
    {
        broadcast_internal(broadcast_packet{ ip_.value(), data });
    }
}
