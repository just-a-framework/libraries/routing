#pragma once

#include <logging/component.hpp>
#include <logging/logging.hpp>

namespace jaf::routing
{
    struct tag
    {
        static constexpr auto component_name = "routing";
    };

    using logger = logging::component<tag>;
}
