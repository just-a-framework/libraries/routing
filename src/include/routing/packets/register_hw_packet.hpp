#pragma once

#include <routing/mask.hpp>
#include <routing/packets/base_packet.hpp>

namespace jaf::routing
{
    struct register_hw_packet
        : base_packet<0>
    {
        uint8_t hwid_ = {0};

        explicit register_hw_packet();
        explicit register_hw_packet(uint8_t hwid);

        serialize::stream& archive(serialize::stream& s) final;
    };

    bool operator==(const register_hw_packet& a, const register_hw_packet& b);
}
