#pragma once

#include <routing/mask.hpp>
#include <routing/packets/base_packet.hpp>

namespace jaf::routing
{
    struct address_config_packet
        : base_packet<2>
    {
        mask m_;

        explicit address_config_packet();
        explicit address_config_packet(mask m);

        serialize::stream& archive(serialize::stream& s) final;
    };

    bool operator==(const address_config_packet& a, const address_config_packet& b);
}
