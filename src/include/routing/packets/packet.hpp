#pragma once

#include <serialize/streamable.hpp>

namespace jaf::routing
{
    using packet_id_t = uint8_t;

    struct packet
        : serialize::streamable
    {
        virtual packet_id_t identifier() const = 0;

        virtual ~packet();
    };
}
