#pragma once

#include <routing/base_router.hpp>

#include <memory>

namespace jaf::routing
{
    struct sub_router
        : base_router
    {
        using base_router::handle;

        sub_router(uint8_t hwid, std::shared_ptr<socket> gateway);
        ~sub_router();

        void handle(socket& s, const data_packet& p) final;
        void handle(socket& s, const broadcast_packet& p) final;

        void send(const ip& receiver, const std::vector<std::byte>& data) final;
        void broadcast(const std::vector<std::byte>& data) final;
    
    private:
        struct gateway_listener
            : signal::listener<socket>
        {
            gateway_listener(sub_router& parent);

            void handle(socket& s, const register_hw_packet& p) final;
            void handle(socket& s, const unregister_hw_packet& p) final;
            void handle(socket& s, const address_config_packet& p) final;
            void handle(socket& s, const failure_config_packet& p) final;
            void handle(socket& s, const data_packet& p) final;
            void handle(socket& s, const broadcast_packet& p) final;

        private:
            sub_router& p_;

            friend class sub_router;
        };

        std::shared_ptr<socket> g_;
        std::shared_ptr<gateway_listener> gl_;
    };
}
