cmake_minimum_required(VERSION 3.19)

if(CONAN_BUILD)
    include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)

    set(PACKAGE_NAME ${CONAN_PACKAGE_NAME})
    set(PACKAGE_VERSION ${CONAN_PACKAGE_VERSION})
else()
    set(PACKAGE_NAME "jaf_routing")
    set(PACKAGE_VERSION "0.0.2")
endif()

set(target_name "routing")

project(${PACKAGE_NAME}
    LANGUAGES CXX
    VERSION ${PACKAGE_VERSION}
)

if(CONAN_BUILD)
    conan_basic_setup()
endif()

set(JAF_COMPONENTS logging;serialize;signal)
find_package(jaf REQUIRED COMPONENTS ${JAF_COMPONENTS})

set(include_dir ${CMAKE_CURRENT_SOURCE_DIR}/include/${target_name})

set(HEADERS
    ${include_dir}/packets/address_config_packet.hpp
    ${include_dir}/packets/base_packet.hpp
    ${include_dir}/packets/broadcast_packet.hpp
    ${include_dir}/packets/data_packet.hpp
    ${include_dir}/packets/failure_config_packet.hpp
    ${include_dir}/packets/packet.hpp
    ${include_dir}/packets/register_hw_packet.hpp
    ${include_dir}/packets/unregister_hw_packet.hpp
    ${include_dir}/base_router.hpp
    ${include_dir}/core_router.hpp
    ${include_dir}/ip.hpp
    ${include_dir}/mask.hpp
    ${include_dir}/router.hpp
    ${include_dir}/socket.hpp
    ${include_dir}/sub_router.hpp
)

set(SOURCES
    packets/address_config_packet.cpp
    packets/broadcast_packet.cpp
    packets/data_packet.cpp
    packets/failure_config_packet.cpp
    packets/packet.cpp
    packets/register_hw_packet.cpp
    packets/unregister_hw_packet.cpp
    base_router.cpp
    core_router.cpp
    ip.cpp
    mask.cpp
    router.cpp
    socket.cpp
    sub_router.cpp
)

add_library(${target_name}
	${SOURCES}
	${HEADERS}
)

target_precompile_headers(${target_name}
    PUBLIC ${include_dir}/pch.hpp
)

set_target_properties(${target_name}
	PROPERTIES OUTPUT_NAME "${target_name}-${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}.${PROJECT_VERSION_PATCH}"
)

target_compile_features(${target_name}
	PUBLIC
		cxx_std_17
)

target_include_directories(${target_name}
	PRIVATE
		include
)

target_include_directories(${target_name}
	PUBLIC
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  		$<INSTALL_INTERFACE:include>
)

target_link_libraries(${target_name}
    PUBLIC
        jaf::logging
        jaf::serialize
        jaf::signal
)

add_library(jaf::${target_name} ALIAS ${target_name})

include(GNUInstallDirs)
include(CMakePackageConfigHelpers)

set(INSTALL_RUNTIME_DIR ${CMAKE_INSTALL_BINDIR})
set(INSTALL_CONFIG_DIR  ${CMAKE_INSTALL_LIBDIR}/jaf/cmake)
set(INSTALL_LIBRARY_DIR ${CMAKE_INSTALL_LIBDIR}/jaf)
set(INSTALL_ARCHIVE_DIR ${CMAKE_INSTALL_LIBDIR}/jaf)
set(INSTALL_INCLUDE_DIR ${CMAKE_INSTALL_INCLUDEDIR})

set(PROJECT_CONFIG_VERSION_FILE "${CMAKE_BINARY_DIR}/jaf-${target_name}-config-version.cmake")
set(PROJECT_CONFIG_FILE         "${CMAKE_BINARY_DIR}/jaf-${target_name}-config.cmake")

configure_package_config_file(cmake/jaf-${target_name}-config.cmake.in
    ${PROJECT_CONFIG_FILE}
    INSTALL_DESTINATION ${INSTALL_CONFIG_DIR}
)

write_basic_package_version_file(
    ${PROJECT_CONFIG_VERSION_FILE}
    COMPATIBILITY SameMajorVersion
)

install(TARGETS ${target_name}
    EXPORT ${target_name}-targets
    RUNTIME DESTINATION ${INSTALL_RUNTIME_DIR}
    LIBRARY DESTINATION ${INSTALL_LIBRARY_DIR}
    ARCHIVE DESTINATION ${INSTALL_ARCHIVE_DIR}
)

install(DIRECTORY include/
    DESTINATION ${INSTALL_INCLUDE_DIR}
)

install(FILES
    ${PROJECT_CONFIG_VERSION_FILE}
    ${PROJECT_CONFIG_FILE}
    DESTINATION ${INSTALL_CONFIG_DIR}
)

install(EXPORT ${target_name}-targets
    FILE jaf-${target_name}-targets.cmake
    NAMESPACE jaf::
    DESTINATION ${INSTALL_CONFIG_DIR}
)

export(EXPORT ${target_name}-targets
    FILE ${CMAKE_CURRENT_BINARY_DIR}/jaf-${target_name}-targets.cmake
    NAMESPACE jaf::
)

export(PACKAGE ${target_name})
