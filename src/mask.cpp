#include <routing/mask.hpp>
#include <routing/logger.hpp>

namespace jaf::routing
{
    mask::mask(ip low, ip high)
        : low_{ std::move(low) }
        , high_{ std::move(high) }
    {
    }

    serialize::stream& mask::archive(serialize::stream& s)
    {
        return (s & low_ & high_);
    }

    std::vector<mask> mask::split(uint32_t count) const
    {
        const auto distance = static_cast<uint64_t>(high_.ip_ - low_.ip_) + 1;
            
        if (distance < count)
        {
            throw std::invalid_argument{"Range too small to divide"};
        }
        else
        {
            const auto part_distance = static_cast<uint32_t>(distance / count);
            std::vector<mask> v{};

            for(auto i = 0u; i < count - 1; ++i)
            {
                const auto& low = ip{ low_.ip_ + i * part_distance };
                const auto& high = ip{ low_.ip_ + (i + 1) * part_distance - 1 };

                v.push_back({ low, high });
                TRACE(logger, low, "-", high);
            }

            const auto& low = ip{ low_.ip_ + (count - 1) * part_distance };
            const auto& high = ip{ high_ };
            v.push_back({ low, high });
            TRACE(logger, low, "-", high);

            return v;
        }
    }
    
    bool mask::contains(const ip& i) const
    {
        return low_ <= i && i <= high_;
    }

    std::ostream& operator<<(std::ostream& os, const mask& m)
    {
        return (os << m.low_ << "-" << m.high_);
    }

    bool operator==(const mask& a, const mask& b)
    {
        return a.high_ == b.high_ && a.low_ == b.low_;
    }

    bool operator<(const mask& a, const mask& b)
    {
        return a.high_ < b.high_ || a.low_ < b.low_;
    }
}
