#include <routing/packets/address_config_packet.hpp>

#include <serialize/streams/vector.hpp>

namespace jaf::routing::test::address_config_packet
{
    struct address_config_packet
        : ::jaf::testing::test_suite
    {
    };

    TEST_F(address_config_packet, archive)
    {
        auto b = std::vector<std::byte>{};
        auto s = ::jaf::serialize::streams::vector{ b };
        
        const auto p1 = ::jaf::routing::address_config_packet{ { {1, 2, 3, 4}, {5, 6, 7, 8} } };
        static_cast<::jaf::serialize::ostream&>(s) & p1;

        auto p2 = ::jaf::routing::address_config_packet{};
        static_cast<::jaf::serialize::istream&>(s) & p2;

        EXPECT_EQ(p1.m_, p2.m_);
    }
}
