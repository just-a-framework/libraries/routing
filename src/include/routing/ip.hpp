#pragma once

#include <serialize/streamable.hpp>

#include <ostream>

namespace jaf::routing
{
    struct ip
        : serialize::streamable
    {
        uint32_t ip_{0};

        explicit ip() = default;
        explicit ip(uint32_t i);

        ip(uint8_t p1, uint8_t p2, uint8_t p3, uint8_t p4);

        serialize::stream& archive(serialize::stream& s) final;

        ip next() const;
    };
    
    std::ostream& operator<<(std::ostream& os, const ip& i);

    bool operator==(const ip& a, const ip& b);
    bool operator!=(const ip& a, const ip& b);
    bool operator<(const ip& a, const ip& b);
    bool operator<=(const ip& a, const ip& b);
    bool operator>(const ip& a, const ip& b);
    bool operator>=(const ip& a, const ip& b);
}
