#include <routing/sub_router.hpp>

#include <routing/logger.hpp>

namespace jaf::routing
{
    sub_router::sub_router(uint8_t hwid, std::shared_ptr<socket> gateway)
        : base_router{ std::move(hwid) }
        , g_{ std::move(gateway) }
        , gl_{ std::make_shared<gateway_listener>(*this) }
    {
        TRACE(logger);

        gl_->add(g_);
        g_->send(register_hw_packet{ hwid_ });
    }

    sub_router::~sub_router()
    {
        TRACE(logger);

        gl_->remove(g_);
        g_->send(unregister_hw_packet{ hwid_ });
    }

    sub_router::gateway_listener::gateway_listener(sub_router& parent)
        : p_(parent)
    {
        TRACE(logger);
    }

    void sub_router::handle(socket& s, const data_packet& p)
    {
        if(!handle_internal(s, p))
        {
            g_->send(p);
        }
    }

    void sub_router::handle(socket& s, const broadcast_packet& p)
    {
        g_->send(p);
    }

    void sub_router::send(const ip& receiver, const std::vector<std::byte>& data)
    {
        if(!send_internal(receiver, data))
        {
            g_->send(data_packet{ ip_.value(), receiver, data});
        }
    }

    void sub_router::broadcast(const std::vector<std::byte>& data)
    {
        g_->send(broadcast_packet{ ip_.value(), data });
    }

    void sub_router::gateway_listener::handle(socket& s, const register_hw_packet& p)
    {
        ERROR(logger, (int)p.hwid_);
    }

    void sub_router::gateway_listener::handle(socket& s, const unregister_hw_packet& p)
    {
        ERROR(logger);
    }

    void sub_router::gateway_listener::handle(socket& s, const address_config_packet& p)
    {
        TRACE(logger);

        p_.configure_subnetworks(p.m_);
    }

    void sub_router::gateway_listener::handle(socket& s, const failure_config_packet& p)
    {
        FATAL(logger);
        throw std::invalid_argument{ "something went wrong" };
    }

    void sub_router::gateway_listener::handle(socket& s, const data_packet& p)
    {
        TRACE(logger, p.sender_, p.receiver_, p.data_.size());

        if(!p_.handle_internal(s, p))
        {
            throw std::invalid_argument{ "ip???" };
        }
    }

    void sub_router::gateway_listener::handle(socket& s, const broadcast_packet& p)
    {
        p_.broadcast_internal(broadcast_packet{ p.sender_, p.data_ });
    }
}
