#include <routing/packets/failure_config_packet.hpp>

namespace jaf::routing
{
    failure_config_packet::failure_config_packet() = default;

    serialize::stream& failure_config_packet::archive(serialize::stream& s)
    {
        return s;
    }

    bool operator==(const failure_config_packet& a, const failure_config_packet& b)
    {
        return true;
    }
}
