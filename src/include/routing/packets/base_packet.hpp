#pragma once

#include <routing/packets/packet.hpp>

namespace jaf::routing
{
    template<packet_id_t id_>
    struct base_packet
        : packet
    {
        static constexpr packet_id_t id = id_;

        virtual packet_id_t identifier() const
        {
            return id;
        }
    };
}
