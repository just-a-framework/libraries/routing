#include <routing/router.hpp>

namespace jaf::routing
{
    router_listener::~router_listener() = default;

    router::~router() = default;
}
