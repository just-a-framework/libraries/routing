#include <routing/base_router.hpp>

#include <routing/logger.hpp>

namespace jaf::routing
{
    base_router::base_router(uint8_t hwid, uint32_t max_clients)
        : hwid_{ std::move(hwid) }
    {
        available_.reserve(max_clients);
    }

    void base_router::handle(socket& s, const register_hw_packet& p)
    {
        TRACE(logger, (int)p.hwid_);

        if(available_.empty())
        {
            FATAL(logger, "out of space");

            s.send(failure_config_packet{});
        }
        else
        {
            INFO(logger, (int)p.hwid_);

            if(const auto recit = std::find_if(used_.begin(), used_.end(), [&](const auto& m){ return m.first == p.hwid_; }); recit == used_.end())
            {
                const auto sl = available_.back();
                available_.pop_back();
                used_.emplace(p.hwid_, socket_t{sl, s});
                s.send(address_config_packet{ sl });
            }
            else
            {
                FATAL(logger, "aleady registered");

                s.send(failure_config_packet{});
            }
        }
    }

    void base_router::handle(socket& s, const unregister_hw_packet& p)
    {
        INFO(logger, (int)p.hwid_);

        if(const auto recit = std::find_if(used_.begin(), used_.end(), [&](const auto& m){ return m.first == p.hwid_; }); recit != used_.end())
        {
            available_.push_back(recit->second.first);
            used_.erase(p.hwid_);
        }
    }

    void base_router::handle(socket& s, const address_config_packet& p)
    {
        TRACE(logger);
    }

    void base_router::handle(socket& s, const failure_config_packet& p)
    {
        ERROR(logger);
    }

    void base_router::configure_subnetworks(mask m)
    {
        TRACE(logger, m);

        ip_ = m.low_;
        m_ = {m.low_.next(), m.high_};

        INFO(logger, "ip: ", ip_.value());

        available_ = m_->split(available_.capacity());
    }

    bool base_router::send_internal(const ip& receiver, const std::vector<std::byte>& data)
    {
        TRACE(logger);

        if(receiver == ip_.value())
        {
            publish(&router_listener::handle, receiver, data);
            return true;
        }
        else if(m_->contains(receiver))
        {
            if(const auto recit = std::find_if(used_.begin(), used_.end(), [&](const auto& m){ return m.second.first.contains(receiver); }); recit != used_.end())
            {
                recit->second.second.get().send(data_packet{ ip_.value(), receiver, data });
                return true;
            }
            else
            {
                throw std::invalid_argument{ "ip???" };
            }
        }
        else
        {
            return false;
        }
    }
    
    void base_router::broadcast_internal(const broadcast_packet& p)
    {
        for(auto& [id, s] : used_)
        {
            s.second.get().send(p);
        }
    }
    
    bool base_router::handle_internal(socket& s, const data_packet& p)
    {
        if(p.receiver_ == ip_.value())
        {
            publish(&router_listener::handle, p.sender_, p.data_);
            return true;
        }
        else if(m_->contains(p.receiver_))
        {
            if(const auto recit = std::find_if(used_.begin(), used_.end(), [&](const auto& m){ return m.second.first.contains(p.receiver_); }); recit != used_.end())
            {
                recit->second.second.get().send(p);
                return true;
            }
            else
            {
                throw std::invalid_argument{ "ip???" };
            }
        }
        else
        {
            return false;
        }
    }
}
