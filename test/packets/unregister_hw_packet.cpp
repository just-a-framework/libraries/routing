#include <routing/packets/unregister_hw_packet.hpp>

#include <serialize/streams/vector.hpp>

namespace jaf::routing::test::unregister_hw_packet
{
    struct unregister_hw_packet
        : ::jaf::testing::test_suite
    {
    };

    TEST_F(unregister_hw_packet, archive)
    {
        auto b = std::vector<std::byte>{};
        auto s = ::jaf::serialize::streams::vector{ b };
        
        const auto p1 = ::jaf::routing::unregister_hw_packet{ 9 };
        static_cast<::jaf::serialize::ostream&>(s) & p1;

        auto p2 = ::jaf::routing::unregister_hw_packet{};
        static_cast<::jaf::serialize::istream&>(s) & p2;

        EXPECT_EQ(p1.hwid_, p2.hwid_);
    }
}
