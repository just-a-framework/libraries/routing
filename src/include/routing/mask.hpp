#pragma once

#include <routing/ip.hpp>
#include <serialize/streamable.hpp>

namespace jaf::routing
{
    struct mask
        : serialize::streamable
    {
        ip low_;
        ip high_;

        explicit mask() = default;

        mask(ip low, ip high);

        serialize::stream& archive(serialize::stream& s) final;

        std::vector<mask> split(uint32_t count) const;
        bool contains(const ip& i) const;
    };

    std::ostream& operator<<(std::ostream& os, const mask& m);
    bool operator==(const mask& a, const mask& b);
    bool operator<(const mask& a, const mask& b);
}
